defmodule CizenDiningPhilosophers do
  @moduledoc """
  Documentation for CizenDiningPhilosophers.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CizenDiningPhilosophers.hello()
      :world

  """
  def hello do
    :world
  end
end
